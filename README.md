# peduli-sehat-h5-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
http://localhost:8080/pedulicare/#/
```

### Compiles and minifies for development
```
npm run build -- --mode development
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
