const router = {
  path: "/",
  component: () => import("@/views/Wrapper.vue"),
  children: [
    {
      path: "",
      name: "GtryClosedIndex",
      component: () => import("@/views/Gtry/Closed/Index.vue")
    },
    {
      path: "gtry/closed",
      component: () => import("@/views/Wrapper.vue"),
      children: [
        {
          path: "",
          name: "GtryClosedIndex2",
          component: () => import("@/views/Gtry/Closed/Index.vue")
        },
        {
          path: "refund",
          name: "GtryRefundIndex",
          component: () => import("@/views/Gtry/Closed/RefundIndex.vue")
        },
        {
          path: "refund/form",
          name: "GtryRefundForm",
          component: () => import("@/views/Gtry/Closed/RefundForm.vue")
        },
        {
          path: "refund/process",
          name: "GtryRefundProcess",
          component: () => import("@/views/Gtry/Closed/RefundProcess.vue")
        }
      ]
    }
  ]
};

export default router;
