const router = {
  path: "/account/gopay",
  component: () => import("@/views/Wrapper.vue"),
  children: [
    {
      path: "",
      name: "GopayAccInformation",
      component: () => import("@/views/Account/Gopay/Index.vue")
    },
    {
      path: "activate",
      name: "GopayAccActivate",
      component: () => import("@/views/Account/Gopay/GopayActivate.vue")
    },
    {
      path: "qr-code/:qrcode",
      name: "GopayAccQrCode",
      component: () => import("@/views/Account/Gopay/QrCode.vue")
    },
    {
      path: "otp",
      name: "GopayAccOtp",
      component: () => import("@/views/Account/Gopay/Otp.vue")
    },
    {
      path: "pin",
      name: "GopayAccPin",
      component: () => import("@/views/Account/Gopay/Pin.vue")
    },
    {
      path: "completed-activate/:status",
      name: "GopayCompletedActivate",
      component: () => import("@/views/Account/Gopay/CompletedActivate.vue")
    }
  ]
};

export default router;
