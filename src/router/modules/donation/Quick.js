const router = {
  path: "/quick-donate",
  component: () => import("@/views/Wrapper.vue"),
  children: [
    {
      path: "",
      name: "QuickDonateIndex",
      component: () => import("@/views/Donation/Quick/Index.vue")
    }
  ]
};

export default router;
