const router = {
  path: "/routine-donation",
  component: () => import("@/views/Wrapper.vue"),
  children: [
    {
      path: "",
      name: "RoutineDonationIndex",
      component: () => import("@/views/Donation/Routine/Index.vue")
    },
    {
      path: "campaign-list",
      name: "RoutineDonationCampaignList",
      component: () => import("@/views/Donation/Routine/CampaignList.vue")
    },
    {
      path: "add/:projectId",
      name: "RoutineDonationAdd",
      component: () => import("@/views/Donation/Routine/Add.vue")
    }
  ]
};

export default router;
