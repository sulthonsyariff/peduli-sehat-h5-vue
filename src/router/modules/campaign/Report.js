const router = {
  path: "/campaign-report",
  component: () => import("@/views/Wrapper.vue"),
  children: [
    {
      path: ":id",
      name: "CampaignReport",
      component: () => import("@/views/Campaign/Report/Index.vue")
    },
    {
      path: "success/:project_id",
      name: "SuccessReport",
      component: () => import("@/views/Campaign/Report/SuccessReport.vue")
    }
  ]
};

export default router;
