const modules = [];
let indexArray = -1;

// gtry
const requireGtry = require.context("./gtry", false, /\.js$/);
requireGtry.keys().forEach(fileName => {
  indexArray++;
  modules.push({});

  modules[indexArray] = {
    ...requireGtry(fileName)
  };

  modules[indexArray] = modules[indexArray].default;
});

// account
const requireAccount = require.context("./account", false, /\.js$/);
requireAccount.keys().forEach(fileName => {
  indexArray++;
  modules.push({});

  modules[indexArray] = {
    ...requireAccount(fileName)
  };

  modules[indexArray] = modules[indexArray].default;
});

// campaign
const requireCampaign = require.context("./campaign", false, /\.js$/);
requireCampaign.keys().forEach(fileName => {
  indexArray++;
  modules.push({});

  modules[indexArray] = {
    ...requireCampaign(fileName)
  };

  modules[indexArray] = modules[indexArray].default;
});

// donation
const requireDonation = require.context("./donation", false, /\.js$/);
requireDonation.keys().forEach(fileName => {
  indexArray++;
  modules.push({});

  modules[indexArray] = {
    ...requireDonation(fileName)
  };

  modules[indexArray] = modules[indexArray].default;
});

export default modules;
