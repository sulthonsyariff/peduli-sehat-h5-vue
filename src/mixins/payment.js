export const payPlatform = {
  methods: {
    getPayPlatform(bankCode) {
      let result;
      switch (bankCode) {
        // gopay
        case "1":
          result = 2;
          break;
        // platformVA => sinarmas VA
        case "2":
          result = 3;
          break;
        // OVO
        case "4":
          result = 4;
          break;
        // DANA
        case "5":
          result = 5;
          break;
        // shopeepay
        case "6":
          result = 6;
          break;
        // faspay
        default:
          result = 1;
          break;
      }

      return result;
    },

    getTotalFee(amount, totalFee) {
      let result;

      // if percentage ex: 0.2, 0.5, 0.007 dll
      if (totalFee < 1) {
        result = Math.trunc(amount / (1 - totalFee)) + 1;
        result -= amount;
      } else {
        result = totalFee;
      }

      return result;
    }
  }
};
