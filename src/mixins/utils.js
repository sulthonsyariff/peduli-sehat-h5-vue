/* eslint-disable no-undef */

// vue-cookies
import "../plugins/vue-cookies";

export function getBrowserDevice() {
  let result;
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    result = "Android-H5";
  } else {
    result = "H5";
  }
  return result;
}

export function amountFormat(number) {
  return Number(number).toLocaleString(["ban", "id"]);
}

export function goToLogin() {
  // eslint-disable-next-line no-undef
  $cookies.remove("passport");

  setTimeout(() => {
    location.href =
      "https://" +
      process.env.VUE_APP_BASE_DOMAIN +
      ".pedulisehat.id/login.html?qf_redirect=" +
      encodeURIComponent(location.href);
  }, 30);
}

export function showNotification(message) {
  this.$store.dispatch("general/showNotification", {
    message: message
  });
}

export function getAge(birthDateString) {
  var today = new Date();
  var birthDate = new Date(birthDateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

// type = main, gtry, asuransi
// location = url_modules
// callback = true ? return : jump to link
export function routeToH5(type, location, callback) {
  let result;
  // main
  if (type === "main") {
    if (process.env.VUE_APP_BASE_DOMAIN === "pre") {
      result = `https://pre.pedulisehat.id/`;
    } else {
      result = `https://pedulisehat.id/`;
    }
  }

  // gtry
  if (type === "gtry") {
    if (process.env.VUE_APP_BASE_DOMAIN === "pre") {
      result = `https://gtry-pre.pedulisehat.id/`;
    } else {
      result = `https://gtry.pedulisehat.id/`;
    }
  }

  // asuransi
  if (type === "asuransi") {
    if (process.env.VUE_APP_BASE_DOMAIN === "pre") {
      result = `https://asuransi-pre.pedulisehat.id/`;
    } else {
      result = `https://asuransi.pedulisehat.id/`;
    }
  }

  const url = result + location;

  if (callback) {
    return url;
  } else {
    setTimeout(() => {
      window.open(url, "_self");
    }, 30);
  }
}

export function ordinalSuffix(i) {
  let switchLanguage = $cookies.isKey("switchLanguage")
    ? $cookies.get("switchLanguage")
    : "";

  if (switchLanguage == "en") {
    let j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  } else {
    return i;
  }
}
