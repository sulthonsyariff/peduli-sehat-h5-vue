import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import modules from "./modules";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  key: "pedulisehat-state",
  storage: window.localStorage,
  modules: ["gopay"]
});

export default new Vuex.Store({
  modules,
  plugins: [vuexLocal.plugin]
});
