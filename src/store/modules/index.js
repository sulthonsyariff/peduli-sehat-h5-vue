import camelCase from "lodash/camelCase";

const modules = {};

// security
const requireSecurity = require.context("./security", false, /\.js$/);
requireSecurity.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireSecurity(fileName)
  };
});

// gtry
const requireGtry = require.context("./gtry", false, /\.js$/);
requireGtry.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireGtry(fileName)
  };
});

// account
const requireAccount = require.context("./account", false, /\.js$/);
requireAccount.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireAccount(fileName)
  };
});

// campaign
const requireCampaign = require.context("./campaign", false, /\.js$/);
requireCampaign.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireCampaign(fileName)
  };
});

// donation
const requireDonation = require.context("./donation", false, /\.js$/);
requireDonation.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireDonation(fileName)
  };
});

// others
const requireOthers = require.context("./others", false, /\.js$/);
requireOthers.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireOthers(fileName)
  };
});

export default modules;
