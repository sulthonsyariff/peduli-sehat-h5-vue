const namespaced = true;

const state = {
  category: [
    "Penyalahgunaan dana",
    "Sudah dicover pihak lain (BPJS, Asuransi)",
    "Memberikan informasi palsu",
    "Benefeciary sudah meninggal",
    "Tidak izin kepada keluarga penerima manfaat",
    "Galang dana tidak relevan",
    "Menggunakan gambar/kata-kata kurang pantas",
    "Spamming (Cyber begger)",
    "Belum ada kabar terbaru",
    "Penggalang dana sedang dalam proses hukum",
    "Target tidak sesuai dengan tipe penyakit (target terlalu tinggi)"
  ]
};

const getters = {
  category: state => state.category
};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  reportCampaign: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PROJECT}/v1/project/report`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  uploadFile: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PROJECT}/v1/upload_file`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
