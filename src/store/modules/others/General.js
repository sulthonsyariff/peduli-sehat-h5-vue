const namespaced = true;

const state = {
  message: "",
  textMessage: "",
  showNotification: false,
  loading: false
};

const getters = {
  message: state => state.message,
  textMessage: state => state.textMessage,
  showNotification: state => state.showNotification,
  loading: state => state.loading
};

const mutations = {
  SET_MESSAGE: (state, payload) => {
    state.message = payload;
  },
  SET_NOTIFICATION: (state, payload) => {
    state.showNotification = payload;
  },
  SET_LOADING: (state, payload) => {
    state.loading = payload;
  }
};

const actions = {
  showNotification({ commit }, payload) {
    let message = payload.message;

    commit("SET_MESSAGE", message);
    commit("SET_NOTIFICATION", true);
  },

  refreshNotification({ commit }) {
    commit("SET_MESSAGE", "");
    commit("SET_NOTIFICATION", false);
  },

  showLoading({ commit }) {
    commit("SET_LOADING", true);
  },

  hideLoading({ commit }) {
    commit("SET_LOADING", false);
  }
};

export { namespaced, state, getters, mutations, actions };
