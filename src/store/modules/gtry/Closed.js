const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchBankList: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PROJECT}/v1/bank`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchBalanceWithdraw: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_HEOUIC}/v1/get_withdraw_balance`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  withdrawRequest: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_HEOUIC}/v1/withdraw_request`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  checkIsWithdraw: ({ commit }, payload) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_HEOUIC}/v1/check_is_withdrawn`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
