const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  gopayLinking: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PASSPORT}/v1/gopay/linking`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  gopayUnlinking: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PASSPORT}/v1/gopay/unlinking`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  gopayBalance: ({ commit }, payload) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PASSPORT}/v1/gopay/balance`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getQrCode: ({ commit }, payload) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PASSPORT}/v1/gopay/qr?data=${payload}`,
        responseType: "arraybuffer"
      })
      .then(response => {
        return (
          `data:${response.headers["content-type"]};base64,` +
          Buffer.from(response.data, "binary").toString("base64")
        );
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
