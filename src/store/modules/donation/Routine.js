const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchListPaymentMethod: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_TRADE}/v2/pay_channel`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
