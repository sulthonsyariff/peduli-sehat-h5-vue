const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchRandomDonation: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PROJECT}/v1/project/getrandom`,
      })
      .then((response) => {
        return response.data.data;
      })
      .catch((error) => {
        throw error.response.data;
      });
  },
};

export { namespaced, state, getters, mutations, actions };
