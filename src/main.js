import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import i18n from "./plugins/vue-i18n";
import "./plugins";
import { showLoading, hideLoading } from "./mixins/loading";
import { showNotification } from "./mixins/utils";
import money from "v-money";

// global component
import BaseNotification from "@/components/Base/Notification";
import BaseLoading from "@/components/Base/Loading";

// otp input
import OtpInput from "@bachdgvn/vue-otp-input";

import "@/assets/css/tailwind.css"; // tailwind styles
import "@/assets/css/custom.scss"; // custom styles

Vue.config.productionTip = false;

Vue.component("base-notification", BaseNotification);
Vue.component("base-loading", BaseLoading);
Vue.component("v-otp-input", OtpInput);
Vue.use(money);

// loading
Vue.prototype.$showLoading = showLoading;
Vue.prototype.$hideLoading = hideLoading;
// notification
Vue.prototype.$showNotification = showNotification;

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
