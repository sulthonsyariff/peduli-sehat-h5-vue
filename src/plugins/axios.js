/* eslint-disable no-undef */
import Vue from "vue";
import store from "../store";
// import router from "../router";
import axios from "axios";
// cookies
import "./vue-cookies";
// mixins
import { getBrowserDevice, goToLogin } from "../mixins/utils";

const _axios = axios.create();

_axios.interceptors.request.use(
  function(config) {
    // passport cookies
    let passport = $cookies.isKey("passport") ? $cookies.get("passport") : "";

    // language cookies
    let language = $cookies.isKey("switchLanguage")
      ? $cookies.get("switchLanguage")
      : "id";

    // check browser device
    let browserDevice = getBrowserDevice();

    config.headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Qsc-Peduli-Token": passport.accessToken,
      Authorization:
        (passport.uid ? passport.uid : "") +
        ":" +
        (passport.signature ? passport.signature : ""),
      ExpiresIn: passport.expiresIn,
      TokenExpires: passport.tokenExpires,
      Platform: browserDevice,
      Version: "",
      "Accept-Language": language
    };
    // }

    return config;
  },
  function(error) {
    // console.log(error);
    return Promise.reject(error);
  }
);

_axios.interceptors.response.use(
  response => {
    // Return a successful response back to the calling service
    return response;
  },
  error => {
    let passport = $cookies.isKey("passport") ? $cookies.get("passport") : "";

    // if not login
    if (!$cookies.isKey("passport")) {
      goToLogin();
    } else {
      // Unauthorized || This is a invalid signature.
      if (error.response.data.code === 401) {
        goToLogin();
      }
      // arrayBuffer for fetch image
      // if login and refresh token
      // Try request again with new token
      else if (
        error.response.data.code === 4011 ||
        error.response.request.responseType === "arraybuffer"
      ) {
        return store
          .dispatch("auth/refreshToken", {
            grantType: "refresh_token",
            refreshToken: passport.refreshToken
          })
          .then(data => {
            $cookies.set(
              "passport",
              JSON.stringify({
                accessToken: data.data.data.accessToken,
                expiresIn: data.data.data.expiresIn,
                refreshToken: data.data.data.refreshToken,
                tokenExpires: data.data.data.tokenExpires,
                uid: data.data.data.uid,
                signature: data.data.data.signature,
                serverTimestamp: new Date().getTime()
              })
            );

            // New request with new token
            const config = error.config;
            // passport cookies
            let passport = $cookies.isKey("passport")
              ? $cookies.get("passport")
              : "";

            // language cookies
            let language = $cookies.isKey("switchLanguage")
              ? $cookies.get("switchLanguage")
              : "id";

            // check browser device
            let browserDevice = getBrowserDevice();

            config.headers = {
              "Content-Type": "application/json; charset=utf-8",
              "Qsc-Peduli-Token": passport.accessToken,
              Authorization:
                (passport.uid ? passport.uid : "") +
                ":" +
                (passport.signature ? passport.signature : ""),
              ExpiresIn: passport.expiresIn,
              TokenExpires: passport.tokenExpires,
              Platform: browserDevice,
              Version: "",
              "Accept-Language": language
            };

            return new Promise((resolve, reject) => {
              axios
                .request(config)
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
          })
          .catch(error => {
            console.log("error refresh token");
            // store.dispatch("auth/clearAppSession");
            // router.push({ name: "Login" });
            return new Promise((resolve, reject) => {
              reject(error);
            });
          });
      } else {
        // Return any error which is not due to authentication back to the calling service
        return new Promise((resolve, reject) => {
          reject(error);
        });
      }
    }
  }
);

Plugin.install = function(Vue) {
  Vue.axios = _axios;
  window.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    }
  });
};

Vue.use(Plugin);

export default Plugin;
