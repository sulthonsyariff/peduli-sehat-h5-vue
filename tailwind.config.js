// tailwind.config.js
module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "app-primary": "#43ac43",
        "app-primary-hover": "#339833",
        "app-secondary": "#ecf6ec",
        "app-orange": "#f09a0a",
        "app-blue-terms": "#51a6ff",
        "app-border": "#eaeaea",
        "app-title": "#333333",
        "app-label": "#292929",
        "app-label-2": "#666666",
        "app-label-3": "#8a8a8a",
        "app-label-4": "#999999",
        "app-error": "#bc0000",
        "app-inactive": "#ff6102",
        "app-input": "#f8f8f8",
        "app-placeholder": "#a9a9a9",
        "app-disabled": "#e5e7eb"
      }
    },
    textColor: theme => ({
      ...theme("colors")
    }),
    backgroundColor: theme => ({
      ...theme("colors")
    })
  },
  variants: {
    backgroundColor: ["odd", "hover"],
    margin: ["last", "first"],
    padding: ["last"]
  }
};
